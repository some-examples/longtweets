class ChatController < WebsocketRails::BaseController
  def new_message
    begin
      chat = PublicChat.create(text: data["message"], user_id: data["user_id"])
    rescue Exception => e
      File.open('tmp/test_stuff.txt', 'w') do |f|
        f.write(e.message)
      end
    end
    broadcast_message :new_message, data
  end
end
